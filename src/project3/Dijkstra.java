package Dijkstra;
import java.util.PriorityQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class Vertex implements Comparable<Vertex>
{
    public final String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public Vertex(String argName) { name = argName; }
    public String toString() { return name; }
    public int compareTo(Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }

}


class Edge
{
    public final Vertex target;
    public final double weight;
    public Edge(Vertex argTarget, double argWeight)
    { target = argTarget; weight = argWeight; }
}

public class Dijkstra
{
    public static void computePaths(Vertex source)
    {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
	vertexQueue.add(source);

	while (!vertexQueue.isEmpty()) {
	    Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
		if (distanceThroughU < v.minDistance) {
		    vertexQueue.remove(v);

		    v.minDistance = distanceThroughU ;
		    v.previous = u;
		    vertexQueue.add(v);
		}
            }
        }
    }

    public static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);

        Collections.reverse(path);
        return path;
    }

    public static void main(String[] args)
    {
    Vertex v0 = new Vertex("Math-Comp Science");
	Vertex v1 = new Vertex("Prince Center");
	Vertex v2 = new Vertex("Torreyson Library");
	Vertex v3 = new Vertex("Lewis Science Center");
	Vertex v4 = new Vertex("College Square");
	Vertex v5 = new Vertex("Burdick");
	Vertex v6 = new Vertex("Speech Language Hearing");
	Vertex v7 = new Vertex("Old Main");
	Vertex v8 = new Vertex("Maintenance College");
	Vertex v9 = new Vertex("Police Dept.");
	Vertex v10 = new Vertex("Fine Art");
	Vertex v11 = new Vertex("McAlister Hall");
	Vertex v12 = new Vertex("Student Center");
	Vertex v13 = new Vertex("Wingo");
	Vertex v14 = new Vertex("Student Health Center");
	Vertex v15 = new Vertex("New Business Building");
	Vertex v16 = new Vertex("Oak Tree Apt.");
	Vertex v17 = new Vertex("Brewer-Hegeman");
	Vertex v18 = new Vertex("Bear Village Apt.");
	v0.adjacencies = new Edge[]{ new Edge(v3,  150),
	                             new Edge(v2,  40),
	                             new Edge(v1, 80),
	                             new Edge(v5, 30)};
	v1.adjacencies = new Edge[]{ new Edge(v4,  300),
								 new Edge(v9, 100),
								 new Edge(v2, 30),
								 new Edge(v0, 80)};
	v2.adjacencies = new Edge[]{ new Edge(v5,  80),
								 new Edge(v7, 30),
								 new Edge(v1,30),
								 new Edge(v0,40)};
	v3.adjacencies = new Edge[]{ new Edge(v6, 250),
			  					 new Edge(v4,200),
			  					 new Edge(v0, 150)};
	v4.adjacencies = new Edge[]{ new Edge(v3, 200),
								 new Edge(v1,300)};
	v5.adjacencies = new Edge[]{ new Edge(v8,  300),
								 new Edge(v11, 200),
								 new Edge(v6, 100),
								 new Edge(v0,30)};
	v6.adjacencies = new Edge[]{ new Edge(v3,  250),
								 new Edge(v5, 100),
								 new Edge(v8, 120)};
	v7.adjacencies = new Edge[]{ new Edge(v2, 30),
								 new Edge(v9, 200),
								 new Edge(v10, 90),
								 new Edge(v11, 100)};
	v8.adjacencies = new Edge[]{ new Edge(v6, 120),
								 new Edge(v5, 300),
								 new Edge(v16, 160),
								 new Edge(v15, 150),
								 new Edge(v13, 100),
								 new Edge(v11, 150)};
	v9.adjacencies = new Edge[]{ new Edge(v1, 100),
								 new Edge(v14, 100),
								 new Edge(v7, 200),
								 new Edge(v10, 50)};
	v10.adjacencies = new Edge[]{new Edge(v7, 90),
								 new Edge(v9, 50),
								 new Edge(v11, 180),
								 new Edge(v12, 80)};
	v11.adjacencies = new Edge[]{new Edge(v12, 100),
								 new Edge(v13, 50),
								 new Edge(v5, 200),
								 new Edge(v8, 150),
								 new Edge(v7, 100),
								 new Edge(v10, 180)};
	v12.adjacencies = new Edge[]{new Edge(v10, 80),
								 new Edge(v11, 100),
								 new Edge(v14, 50),
								 new Edge(v13, 100),
								 new Edge(v15, 110)};
	v13.adjacencies = new Edge[]{new Edge(v12, 100),
			 					 new Edge(v15, 50),
			 					 new Edge(v8, 100),
			 					 new Edge(v11, 50)};
	v14.adjacencies = new Edge[]{new Edge(v12, 50),
								 new Edge(v17, 200),
								 new Edge(v9, 100)};
	v15.adjacencies = new Edge[]{new Edge(v13, 50),
								 new Edge(v12, 110),
								 new Edge(v8, 150),
								 new Edge(v16, 30)};
	v16.adjacencies = new Edge[]{new Edge(v15, 30),
								 new Edge(v8, 160),
								 new Edge(v17, 40)};
	v17.adjacencies = new Edge[]{new Edge(v16, 40),
								 new Edge(v15, 20),
								 new Edge(v18, 350),
								 new Edge(v14, 200)};
	v18.adjacencies = new Edge[]{new Edge(v17, 350)};
	
	Vertex[] vertices = { v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, v18 };

        computePaths(v0);
        for (Vertex v : vertices)
	{
	    System.out.println("Distance to " + v + ": " + v.minDistance);
	    List<Vertex> path = getShortestPathTo(v);
	    System.out.println("Path: " + path);
	}
    }
}
